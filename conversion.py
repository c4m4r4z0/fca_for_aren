import json

#Charge le fichier json et l'enregistre dans un dictionnaire
with open('6.json') as json_data:
    data_dict = json.load(json_data)

#print(data_dict["comments"][0]["reformulation"]) #Les commentaires sont une liste, on indique le numéro et l'attribut du commentaire souhaité

#création de la table support à l'algo FCA
items=[]
table={}
for i in data_dict["comments"]:
    #Les entrées sont les propos du débat
    clef=i["reformulation"].replace('\n','')
    table[clef]=[]
    cles=i.keys()
    #Les valeurs sont les tags associés, qu'ils soient proposés par l'utilisateur ou par Jeu2Mots
    if "tags" in cles :
        for j in i["tags"]:
            valeur=j["value"].replace(' ','_')
            table[clef].append(valeur)
            if not(valeur in items):
                items.append(valeur)
    for j in i["proposedTags"]:
        valeur=j["value"].replace(' ','_')
        table[clef].append(valeur)
        if not(valeur in items):
            items.append(valeur)

fileFormat=str(input("Quelle format de fichier voulez-vous ? (cxt ou slf): "))
if fileFormat=="cxt" :
#Création du fichier .cxt
#Création de l'entête (format, nbre objets, nbre items)
    fout=open("output.cxt","w")
    s="B\n\n"+str(len(table))+'\n'+str(len(items))+"\n\n"
    fout.write(s)
    #Ecriture de tous les objets
    s=""
    for o in table:
        s=s+str(o)+'\n'
    fout.write(s)
    #Ecriture de tous les items
    s=""
    for i in items:
        s=s+str(i)+'\n'
    fout.write(s)
    #Ecriture de la matrice
    s=""
    for o in table:
        for i in items:
            if i in table[o]:
                s=s+"X"
            else:
                s=s+"."
        s=s+"\n"
    fout.write(s)
    fout.close()
elif fileFormat=="slf" :
    #Ecriture de l'en-tête
    fout=open("output.slf", "w")
    s="[Lattice]\n"+str(len(table))+'\n'+str(len(items))+'\n'+"[Objects]\n"
    fout.write(s)
    #Ecriture des objets
    s=""
    for o in table:
        s=s+"Object "+str(o)+'\n'
    s=s+"[Attributes]\n"
    fout.write(s)
    #Ecriture des objets
    s=""
    for i in items:
        s=s+"Attribute "+str(i)+'\n'
    fout.write(s)
    #Ecriture de la matrice
    s="[relation]\n"
    for o in table:
        for i in items:
            if i in table[o]:
                s=s+"1"
            else:
                s=s+"0"
        s=s+"\n"
    fout.write(s)
    fout.close()
