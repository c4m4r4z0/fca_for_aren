#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from collections import OrderedDict

#Charge le fichier json et l'enregistre dans un dictionnaire
with open('6.json') as json_data:
    data_dict = json.load(open("6.json","rb"), encoding="utf-8")

#print(data_dict["comments"][0]["reformulation"]) #Les commentaires sont une liste, on indique le numéro et l'attribut du commentaire souhaité

#création de la table support à l'algo FCA
items=[]
table={}
for i in data_dict["comments"]:
    #Les entrées sont les propos du débat
    clef=i["reformulation"].replace('\n','')
    table[clef]=[]
    cles=i.keys()
    #Les valeurs sont les tags associés, qu'ils soient proposés par l'utilisateur ou par JeuxDeMots
    if "tags" in cles :
        for j in i["tags"]:
            valeur=j["value"].replace(' ','_')
            table[clef].append(valeur)
            if not(valeur in items):
                items.append(valeur)
    for j in i["proposedTags"]:
        valeur=j["value"].replace(' ','_')
        table[clef].append(valeur)
        if not(valeur in items):
            items.append(valeur)

#Création de la table d'objets
objets=[]
for o in table:
    objets.append(o)

#Renvoie l'intersection de 2 listes
def inter(l1, l2):
    l=[]
    if l1==[] or l2==[]:
        return []
    for i in l1:
        if i in l2:
            l.append(i)
    return l

#renvoie l'intersection entre n listes contenu dans une liste. L'argument est donc une liste de listes
def interrec(l):
    if len(l)==1:
        return l[0]
    if len(l)==2:
        return inter(l[0], l[1])
    else:
        return inter(l[0],interrec(l[1:]))

#Renvoie la liste donnée en argument sans doublon
def sansDoublon(l):
    l1=[]
    for i in l:
        if not(i in l1):
            l1.append(i)
    return l1

#Renvoie la liste l1 privé des éléments qu'elle partage avec l2
def disjoin(l1, l2):
    l=[]
    for i in l1:
        if not(i in l2):
            l.append(i)
    return l

#Renvoie Les attributs partagés par tous les objets contenus dans E
def A(E, contexte):
    attributs=[]
    if E==[]:
        for o in contexte:
            attributs.append(contexte[o])
        return sansDoublon(attributs)
    l=[]
    for o in E:
        l=contexte[o]
        attributs.append(l)
    return interrec(attributs)

#Renvoie les objets vérifiant tous les attributs contenus dans A
def E(A, contexte):
    obj=[]
    if A==[]:
        for o in contexte:
            obj.append(o)
        return obj
    for o in contexte:
        b=True
        for a in A :
            if not(a in contexte[o]):
                   b=False
        if b:
            obj.append(o)
    return obj          

#Renvoie A(E([a]))
def fermeture(a, contexte):
    return A(E([a], contexte), contexte)

#Vérifie en temps polynomial si l'attribut a est un irréductible.
def irr(a, objets, items, contexte):
    F=fermeture(a, contexte)
    F=disjoin(F,[a])
    Ot=disjoin(objets, E([a], contexte))
    fusion=[]
    for ai in F:
        if Ot==[] :
            return False
        if E([a], contexte) == E([ai], contexte) :
            fusion.append(ai)
        if not(ai in fusion) :
            Ot=inter(Ot, E([ai], contexte))
    return Ot != []

#renvoie une liste des attributs identiques
def chercheIdentique(items, contexte) :
    l=[]
    k=1
    for i in items :
        #Si i a déjà été détecté commme étant un doublon, on ne le traite pas
        if not(i in l) :
            #On cherche dans leds attributs qui n'ont pas été visité, et qui ne sont pas des doublons
            nl=items[k:]
            nl=disjoin(nl, l)
            for j in nl:
                if E([i],contexte)==E([j], contexte) :
                    l.append(j)
        k=k+1
    return l

#renvoi des irréductibles distincts
def distIrr(objets, items, contexte) :
    l=[]
    identiques=chercheIdentique(items, contexte)
    att=disjoin(items,identiques)
    for a in att :
        if irr(a, objets, att, contexte) :
            l.append(a)
    return l

#Renvoie tous les irréductibles présents dans la fermeture d'un attribut
def chercheIrr(a, objets, items, contexte):
    F=fermeture(a, contexte)
    F=sansDoublon(F)
    F=disjoin(F,[a])
    l=[]
    for ai in F:
        if irr(ai, objets, items, contexte):
            l.append(ai)
    return l

#renvoie un dictionnaire tel que les entrées sont des irréductibles, et les valeurs
#des listes d'irréductibles. Le dictionnaire exprimer des relations clé => valeurs
def implication(irreductibles, objets, items, contexte):
    dico={}
    for a in irreductibles:
        l=chercheIrr(a, objets, items, contexte)
        if l != []:
            dico[a]=l
    return dico

#Renvoie l'attribut qui couvre le plus d'objet
def couvreurMax(couvrir, objets, items, contexte) :
    maxim=items[0]
    n=0
    for a in items :
        #un attribut a couvre E-E(a)
        couverture=disjoin(objets, E([a], contexte))
        intersec=inter(couverture, couvrir)
        x=len(intersec)
        if x>n :
            n=x
            maxim=a
    if n==0 :
        return "noone"
    else :
        return maxim

#renvoie la liste d'attributs qui couvrent au moins en partie objets
def couvreurListe(couvrir, objets, items, contexte):
    l=[]
    for a in items :
        couverture=disjoin(objets, E([a],contexte))
        if inter(couvrir, couverture) != [] :
            l.append(a)
    return l

#Renvoie la liste des attributs qui couvrent les objets de objets
def couverture(couvrir, objets, items, contexte) :
    l=[]
    while couvrir != [] :
        c=couvreurMax(couvrir, objets, items, contexte)
        if c == "noone" :
            return []
        else :
            l.append(c)
            o=disjoin(objets, E([c], contexte))
            couvrir=disjoin(couvrir, o)
    return l

#Renvoi la liste des a1..an => b tel que b appartient à attributs et ai appartient à items
def implication1(attributs, objets, items, contexte) :
    dico={}
    for a in attributs:
        att=disjoin(items, [a])
        #ensemble à couvrir E-E(a)
        o=disjoin(objets, E([a], contexte))
        l=couverture(o, objets, att, contexte)
        if l != [] :
            dico[a]=l
    return dico

#Renvoi des listes de listes des a1..an => b tel que b appartient à attributs et ai appartient à items
def implications2(attributs, objets, items, contexte):
    dico={}
    for a in attributs :
        att=disjoin(items, [a])
        couvrir=disjoin(objets, E(a, contexte))
        lc=couvreurListe(couvrir, objets, att, contexte)
        for ai in lc :
            #on enlève des objets à couvrir les objets couvert par !ai
            o=disjoin(couvrir, disjoin(objets, E([ai], contexte)))
            #Si o est déjà vide, plus d'objets à couvrir
            if o ==[] :
                #On vérifie si la clé existe déjà
                if a in dico :
                    dico[a].append([ai])
                else :
                    dico[a]=[[ai]]
            #Sinon on appelle la fonction couverture sur le reste des objets
            else :
                tempo=disjoin(att, [ai])
                l=couverture(o, objets, tempo, contexte)
                if l != [] :
                    l.append(ai)
                    if a in dico :
                        dico[a].append(l)
                    else :
                        dico[a]=[l]
    return dico        
        
def simplification(dico, liste) :
    l=liste
    for e in liste :
        if e in dico and e in l:
            for atome in dico[e] :
                if atome in l :
                    l.remove(atome)
    return l

#contexte simple pour manipuler les opérations
a=['1','2','3','4','5','6','7','8']
o=['a','b','c','d','e','f','g','h']
d={}
d['a']=['2','4','5','6','8']
d['b']=['1','3','5','7']
d['c']=['3','6']
d['d']=['1','2','3','4']
d['e']=['5','6','7','8']
d['f']=['1','4','7']
d['g']=['2','5','8']
d['h']=['2','3','5','7']
f=open("test.txt","w")
regle=implication1(a, o, a, d)
s=""
for i in regle :
    s=""
    for j in regle[i] :
        s=s+str(j)+", "
    s=s[:len(s)-2]+" => "+str(i)+"\n"
    f.write(s)
f.write('\n')
regle1=implications2(a, o, a, d)
for i in regle1 :
    for j in regle1[i] :
        s=""
        for k in j :
            s=s+str(k)+", "
        s=s[:len(s)-2]+" => "+str(i)+"\n"
        f.write(s)
f.close()

irreductibles=[]
for a in items:
    if irr(a, objets, items, table):
        irreductibles.append(a)
d_irr=distIrr(objets, items, table)
print("nombre d'attributs : "+str(len(items)))
print("nombre d'irréductibles : "+str(len(irreductibles)))
print("nombre d'irréductibles distincts : "+str(len(d_irr)))

relations=implication(d_irr, objets, items, table)
relations=OrderedDict(sorted(relations.items(), key=lambda t: t[0]))
print("nombre de relations : "+str(len(relations)))
for r in relations :
    relations[r]=simplification(relations, relations[r])
    
relations1=implication1(d_irr, objets, d_irr, table)
relations1=OrderedDict(sorted(relations1.items(), key=lambda t: t[0]))
print("nombre de relations (1) : "+str(len(relations1)))
for r in relations1 :
    relations1[r]=simplification(relations, relations1[r])

fichier=open("relations.txt","w")
s="nombre d'attributs : "+str(len(items))+"\nnombre d'irréductibles : "+str(len(irreductibles))+"\nnombre d'irréductibles distincts : "+str(len(d_irr))+"\n\n"
fichier.write(s)
s=""
for i in d_irr :
    s=s+str(i)+"\n"
s=s+"\nnombre de relations : "+str(len(relations))+"\n"
fichier.write(s)
s=""
for i in relations:
    s=str(i)+" => "
    for j in relations[i]:
        s=s+str(j)+", "
    s=s[:len(s)-2]+"\n"
    fichier.write(s)
s="\nnombre de relations (1) : "+str(len(relations1))+"\n"
fichier.write(s)
s=""
for i in relations1 :
    s=""
    for j in relations1[i] :
        s=s+str(j)+", "
    s=s[:len(s)-2]+" => "+str(i)+"\n"
    fichier.write(s)
fichier.close()

relations2=implications2(d_irr, objets, d_irr, table)
relations2=OrderedDict(sorted(relations2.items(), key=lambda t: t[0]))
n=0
for entree in relations2 :
    for e in relations2[entree]:
        n=n+1
print("nombre de relations (2) : "+str(n))
for i in relations2 :
    for j in range(len(relations2[i])) :
        relations2[i][j]=simplification(relations, relations2[i][j])

newFile=open("relationSup.txt","w")
s="nombre de relations : "+str(n)+"\n\n"
newFile.write(s)
s=""
for i in relations2 :
    for j in relations2[i] :
        s=""
        for k in j :
            s=s+str(k)+", "
        s=s[:len(s)-2]+" => "+str(i)+"\n"
        newFile.write(s)
newFile.close()
