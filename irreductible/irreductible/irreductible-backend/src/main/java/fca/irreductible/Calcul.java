package fca.irreductible;

import java.io.IOException;
import java.io.File;

public class Calcul {
	public static void main(String[] args) throws IOException {
		//Construit le contexte à partir des informations contenus dans le json : 6.json
		File input=new File("6.json");
		String path=input.getCanonicalPath();
		Contexte debat=new Contexte(path);
		//Création d'un contexte manuelle (sans fichier) pour tester les différentes opérations
		/*Contexte test=new Contexte();
		
		Objet o1=new Objet(test, "a", 0); Objet o2=new Objet(test, "b", 1); Objet o3=new Objet(test, "c", 2); Objet o4=new Objet(test, "d", 3);
		Objet o5 = new Objet(test, "e", 4); Objet o6 = new Objet(test, "f", 5); Objet o7 = new Objet(test, "g", 6); Objet o8=new Objet(test, "h", 7);
		ArrayList<Objet> lo=new ArrayList<Objet>(Arrays.asList(o1,o2,o3,o4,o5,o6,o7,o8));
		
		Attribut a1=new Attribut(test, "1", 0); Attribut a2=new Attribut(test, "2", 1); Attribut a3=new Attribut(test, "3", 2); Attribut a4=new Attribut(test, "4", 3);
		Attribut a5=new Attribut(test, "5", 4); Attribut a6=new Attribut(test, "6", 5); Attribut a7=new Attribut(test, "7", 6); Attribut a8=new Attribut(test, "8", 7);
		ArrayList<Attribut> la=new ArrayList<Attribut>(Arrays.asList(a1,a2,a3,a4,a5,a6,a7,a8));
		
		ArrayList<Attribut> r1=new ArrayList<Attribut>(Arrays.asList(a2, a4, a5, a6, a8));
		ArrayList<Attribut> r2=new ArrayList<Attribut>(Arrays.asList(a1, a3, a5, a7));
		ArrayList<Attribut> r3=new ArrayList<Attribut>(Arrays.asList(a3, a6));
		ArrayList<Attribut> r4=new ArrayList<Attribut>(Arrays.asList(a1, a2, a3, a4));
		ArrayList<Attribut> r5=new ArrayList<Attribut>(Arrays.asList(a5, a6, a7, a8));
		ArrayList<Attribut> r6=new ArrayList<Attribut>(Arrays.asList(a1, a4, a7));
		ArrayList<Attribut> r7=new ArrayList<Attribut>(Arrays.asList(a2, a5, a8));
		ArrayList<Attribut> r8=new ArrayList<Attribut>(Arrays.asList(a2, a3, a5, a7));
		
		HashMap<Objet, ArrayList<Attribut>> map=new HashMap<Objet, ArrayList<Attribut>>();
		map.put(o1, r1); map.put(o2, r2); map.put(o3, r3); map.put(o4, r4);
		map.put(o5, r5); map.put(o6, r6); map.put(o7, r7); map.put(o8, r8);
		test.setAttributs(la); test.setObjets(lo); test.setContexte(map);*/
		//System.out.println(test.valuesImpKey(la, la));
		/*ArrayList<Objet> lob=new ArrayList<Objet>(); lob.add(o1); lob.add(o2); lob.add(o3);
		System.out.println(o1.a());
		System.out.println(a4.e());
		System.out.println(test.get("a"));
		System.out.println(test.a(lob));
		System.out.println(a1.fermeture());
		String [] n= {"a","b"};
		System.out.println(test.a(n));
		ArrayList<ArrayList<Attribut>> lr=new ArrayList<ArrayList<Attribut>>(); lr.add(r1); lr.add(r7); lr.add(r8);
		System.out.println(test.disjoin(r2, r6));
		System.out.println(test.interrec(lr));
		System.out.println(debat.objets().size());
		System.out.println(debat.attributs().size());
		System.out.println(debat.irreductibles().size());
		System.out.println(debat.distIrr().size());*/
		Ecriture ecr=new Ecriture(debat);
		ecr.ecrireJson();
		ecr.ecrireTxt("c");
	}
}