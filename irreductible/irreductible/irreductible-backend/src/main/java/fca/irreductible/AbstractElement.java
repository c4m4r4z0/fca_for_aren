package fca.irreductible;

/**
 * 
 * @author CAMARAZO David
 * @class Attribut
 * @brief Cette classe abstraite représente un élément d'un contexte FCA, soit un attribut, soit un objet
 * @details Elle permet de définir des méthodes communes aux attributs et aux objets, de sorte à avoir une certaine généricité dans le code
 */
public abstract class AbstractElement extends AbstractFCA implements Comparable<AbstractElement>{
	public abstract String nom();
	
	/**
	 * @brief Méthode permettant de comparer des éléments FCA
	 * @param e l'élément avec lequel on compare
	 * @return -1 si l'appelant est inférieur au paramètre e, 1 s'il est supérieur, 0 sinon
	 */
	public int compareTo(AbstractElement e) {
		return this.nom().compareTo(e.nom());
	}
	
	/**
	 * @brief Réecriture de la méthode equals pour les objets FCA
	 * @param e un élément dont on souhaite vérifier l'égalité avec l'instance appelante
	 * @return true si e a le même nom que l'instance appelante
	 */
	public <T extends AbstractElement> boolean equals(T e) {
		return this.nom().equals(e.nom());
	}
}
