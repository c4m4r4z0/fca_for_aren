package fca.irreductible;

import java.util.ArrayList;
import java.util.Set;
import java.util.Collections;
/**
 * 
 * @author CAMARAZO David
 * @class AbstractFCA
 * @brief Cette classe abstraite contient des méthodes utiles en FCA.
 * @details Cette classe contient de nombreuses méthodes pour manipuler plus simplement des ensembles, elles seront utilisés par les différents composant de la FCA, les objets, les attributs et les contextes
 */
public abstract class AbstractFCA {
	
	/**
	 * @brief Méthode permettant de déterminer si un objet ou un attribut est présent dans une liste.
	 * @details La méthode considère que e est dans l, si un élément i de l a le même nom que e.
	 * @note Le type de l'élément e, et le type des éléments de l doit être le même et doit être une sous-classe de AbstractElement
	 * @param l La liste dont on cherche à savoir si elle contient e
	 * @param e L'élément dont à cherche à déterminer s'il appartient à l
	 * @return true si e appartient à l, false sinon
	 */
	public <T extends AbstractElement> boolean contient(ArrayList<T> l, T e) {
		for(T i : l) {
			if(e.nom().equals(i.nom())) { return true;}
		}
		return false;
	}
	
	/**
	 * @brief Méthode permettant d'obtenir l'intersection entre 2 listes.
	 * @note Le type des éléments de l1 et le type des éléments de l2 doivent être le même.
	 * @param l1 la liste dont on souhaite calculer l'intersection avec l2
	 * @param l2 la liste dont on souhaite calculer l'intersection avec l1
	 * @return l'intersection entre l1 et l2
	 */
	public <T extends AbstractElement> ArrayList<T> inter(ArrayList<T> l1, ArrayList<T> l2) {
		ArrayList<T> res=new ArrayList<T>();
		for(T e : l1) {
			if(contient(l2, e)) {
				res.add(e);
			}
		}
		return res;
	}
	/**
	 * @brief Permet d'obtenir l'intersection de plusieurs listes contenus dans une liste
	 * @note Le type T des éléménts des listes de l doivent êtres des sous-types de AbstractElement
	 * @param l une liste contenant les listes dont on souhaite calculer l'intersection
	 * @return l'intersection des listes contenus dans l.
	 */
	public <T extends AbstractElement> ArrayList<T> interrec(ArrayList<ArrayList<T>> l){
		if(l.size()==1) {return l.get(0);}
		else if(l.size()==2) {return inter(l.get(0), l.get(1));}
		else {
			ArrayList<T> tete=l.get(0);
			l.remove(tete);
			return inter(tete, interrec(l));
		}
	}
	/**
	 * @brief Permet de supprimer les doublons d'une liste
	 * @note Le type T des éléments de l doit être un sous-type de AbstractElement
	 * @param l la liste dont on souhaite supprimer les doublons
	 * @return la liste l privé de doublons
	 */
	public <T extends AbstractElement> ArrayList<T> sansDoublon(ArrayList<T> l) {
		ArrayList<T> res=new ArrayList<T>();
		for(T e : l) {
			if(!contient(res, e)) {res.add(e);}
		}
		return res;
	}
	/**
	 * @brief Permet de supprimer les éléments d'une liste dans une autre liste
	 * @details Si la seconde liste contient des éléments qui ne sont pas dans la première, ces éléments sont ignorés (rien ne se passe)
	 * @note Le type T des éléments de l1 et de l2 doit être le même et doit être un sous-type de AbstractElement
	 * @param l1 la liste dont on souhaite supprimer les éléments de l2
	 * @param l2 la liste contenant des éléments qu'on veut supprimer de l1
	 * @return La liste l1 privé de l'intersection entre l1 et l2
	 */
	public <T extends AbstractElement> ArrayList<T> disjoin(ArrayList<T> l1, ArrayList<T> l2){
		ArrayList<T> res=new ArrayList<T>();
		for(T e : l1) {
			if(!contient(l2, e)) {res.add(e);}
		}
		return res;
	}
	
	/**
	 * @brief Teste l'égalité entre deux listes
	 * @details Attention, ici on considère que 2 listes sont égales si elles ont la même taille et contiennent les mêmes éléments, peu importe le nombre d'occurence de ces éléments
	 * @note Le type T des éléments de l1 et de l2 doit être le même et doit être un sous-type de AbstractElement
	 * @param l1 la liste dont on veut déterminer si elle est égal à l2
	 * @param l2 la liste dont on veut déterminer si elle est égal à l1
	 * @return true si les deux listes ont la même taille et les mêmes éléments, false sinon
	 */
	public <T extends AbstractElement> boolean egal(ArrayList<T> l1, ArrayList<T> l2) {
		if(l1.size() != l2.size()) { return false;}
		for(T e : l1) {
			if(!contient(l2, e)) { return false; }
		}
		return true;
	}
	/**
	 * @brief Permet d'enlever un élément d'une liste
	 * @note Le type T des éléments de l1 et de e doivent être le même et doit être un sous-type de AbstractElement
	 * @param l la liste dont on souhaite supprimer e
	 * @param e l'élément qu'on souhaite supprimer de l
	 * @return la liste l privé de e
	 */
	public <T extends AbstractElement> ArrayList<T> enleve(ArrayList<T> l, T e){
		ArrayList<T> res=new ArrayList<T>();
		for(T x : l) {
			if(!(x.nom().equals(e.nom()))) {res.add(x);}
		}
		return res;
	}
	/**
	 * @brief Méthode permettant de convertir un Set (ensemble) d'éléments FCA en une liste triée par ordre alphabétique
	 * @param s le Set que l'on souhaite convertir en liste triée
	 * @return une liste contenant tous les éléments de l'ensemble rangés par ordre alphabétique
	 */
	public <T extends AbstractElement> ArrayList<T> triSet(Set<T> s){
		ArrayList<T> list = new ArrayList<T>(s);
		Collections.sort(list);
		return list;
	}
}