package fca.irreductible;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Set;
import java.io.File;
/**
 * 
 * @author CAMARAZO David
 * @class Contexte
 * @brief Cette classe représente un contexte en FCA
 * @details 5 attributs : la liste d'objets contenant les objets du contexte, la liste des attributs contenant les attributs du contexte,
 * @details une table donnant les attributs vérifiés par chaque objet, une table donnant les objets qui vérifient un attribut donné, un identifiant
 */
public class Contexte extends AbstractFCA{
	private ArrayList<Objet> objets=new ArrayList<Objet>();
	private ArrayList<Attribut> attributs=new ArrayList<Attribut>();
	private HashMap<Objet, ArrayList<Attribut>> contexte=new HashMap<Objet, ArrayList<Attribut>>();
	private HashMap<String, ArrayList<Objet>> revContexte=new HashMap<String, ArrayList<Objet>>();
	private int id=0;
	
	/**
	 * @brief Constructeur par défaut du contexte
	 * @note Ce constructeur n'affecte aucune valeur aux attributs, il sert juste à créer un contexte vide destiné à être rempli par la suite
	 */
	public Contexte() {
	}
	/**
	 * @brief Constructeur paramétré pour un contexte, l'identifiant n'est pas à renseigné
	 * @param obj est la liste d'objets du contexte
	 * @param att est la liste d'attributs du contexte
	 * @param map est la table associant les attributs vérifiés par chaque objet
	 */
	public Contexte(ArrayList<Objet> obj, ArrayList<Attribut> att, HashMap<Objet, ArrayList<Attribut>> map) {
		objets=obj;
		attributs=att;
		contexte=map;
	}
	/**
	 * @brief Constructeur paramétré pour créer un contexte à partir des informations contenus dans un json
	 * @param file le nom du fichier json qui contient les informations servant à créer le contexte lié au débat
	 * @throws IOException
	 */
	public Contexte(String file) throws IOException {
		int n_objet=0;
		int n_att=0;
		ObjectMapper mapper=new ObjectMapper();
		File fichier = new File(file);
		JsonNode node = mapper.readValue(fichier, JsonNode.class);
		JsonNode comments=node.get("comments");
		Iterator<JsonNode> c=comments.elements();
		while(c.hasNext()) {
			JsonNode comment=c.next();
			String clef=comment.get("reformulation").textValue();
			clef=clef.replace("\n","");
			Objet o = new Objet(this ,clef, n_objet);
			if(!contient(objets, o)) {
				objets.add(o);
			}
			ArrayList<Attribut> listeAtt=new ArrayList<Attribut>();
			if(comment.has("tags")){
				JsonNode tags=comment.get("tags");
				Iterator<JsonNode> t=tags.elements();
				while(t.hasNext()) {
					JsonNode tag=t.next();
					String valeur=tag.get("value").textValue();
					Attribut a=new Attribut(this, valeur, n_att);
					listeAtt.add(a);
					if(!contient(attributs, a)) {
						attributs.add(a);
						n_att=n_att+1;
						revContexte.put(valeur, new ArrayList<Objet>());
					}
					ArrayList<Objet> lo=revContexte.get(valeur);
					if(!contient(lo,o)) lo.add(o);
					revContexte.put(valeur, lo);
				}
			}
			if(comment.has("proposedTags")) {
				JsonNode tags=comment.get("proposedTags");
				Iterator<JsonNode> t=tags.elements();
				while(t.hasNext()) {
					JsonNode tag=t.next();
					String valeur=tag.get("value").textValue();
					Attribut a=new Attribut(this, valeur, n_att);
					listeAtt.add(a);
					if(!contient(attributs, a)) {
						attributs.add(a);
						n_att=n_att+1;
						
						revContexte.put(valeur, new ArrayList<Objet>());
					}
					ArrayList<Objet> lo=revContexte.get(valeur);
					if(!contient(lo,o)) lo.add(o);
					revContexte.put(valeur, lo);
				}
			}
			contexte.put(o, listeAtt);
		}
	}
	/**
	 * @brief Méthode permettant d'affecter une liste d'objets au contexte
	 * @param o la liste d'objets à affecter au contexte
	 */
	public void setObjets(ArrayList<Objet> o) {
		objets=o;
	}
	/**
	 * @brief Méthode permettant d'affecter une liste d'attributs au contexte
	 * @param a la liste d'attributs à affecter au contexte
	 */
	public void setAttributs(ArrayList<Attribut> a) {
		attributs=a;
	}
	/**
	 * @brief Méthode permettant d'affecter une map représétant le contexte
	 * @details contexte est une map donnant pour chaque objet la liste d'attributs vérifiés par cet objet
	 * @param c la map représentant le contexte
	 */
	public void setContexte(HashMap<Objet,ArrayList<Attribut>> c) {
		contexte=c;
	}
	/**
	 * @brief Permet d'affecter un identifiant au contexte
	 * @param i un entier qui peut servir d'identifiant au contexte
	 */
	public void setNumero(int i) {
		id=i;
	}
	/**
	 * @brief Accesseur permettant de récupérer les objets du contexte
	 * @return objets la liste d'objets du contexte
	 */
	public ArrayList<Objet> objets(){
		return objets;
	}
	/**
	 * @brief Accesseur permettant de récupérer les attributs du contexte
	 * @return attributs la liste d'attributs du contexte
	 */
	public ArrayList<Attribut> attributs(){
		return attributs;
	}
	/**
	 * @brief Accesseur permettant de récupérer les attributs vérifiés par un objet du contexte
	 * @param o l'objet dont on souhaite récupérer les attributs
	 * @return la liste des attributs vérifiés par o dans le contexte
	 */
	public ArrayList<Attribut> get(Objet o){
		return contexte.get(o);
	}
	/**
	 * @brief Accesseur permettant de récupérer les attributs vérifiés par un objet du contexte
	 * @param o le nom de l'objet dont on souhaite récupérer les attributs
	 * @return la liste des attributs vérifiés par l'objet dont le nom correspond à o
	 */
	public ArrayList<Attribut> get(String o){
		for(Objet obj : objets) {
			if(obj.nom().equals(o)) {
				return contexte.get(obj);
			}
		}
		return null;
	}
	/**
	 * @brief Permet de récupérer l'objet correspondant à un nom
	 * @param s le nom d'un objet
	 * @return l'objet ayant comme nom s s'il y en a un dans le contexte, null sinon
	 */
	public Objet getObjet(String s) {
		for(Objet o : objets) {
			if(o.nom().equals(s)) {
				return o;
			}
		}
		return null;
	}
	/**
	 * @brief Permet de récupérer l'attribut correspondant à un nom
	 * @param s le nom d'un attribut
	 * @return l'attribut ayant comme nom s s'il y en a un dans le contexte null sinon
	 */
	public Attribut getAttribut(String s) {
		for(Attribut a : attributs) {
			if(a.nom().equals(s)) {
				return a;
			}
		}
		return null;
	}
	/**
	 * @brief Applique l'opération A sur une liste d'objets
	 * @param objs une liste d'objets sur laquelle on veut effectuer l'opération A
	 * @return la liste d'attributs vérifiés par tous les objets de objs
	 */
	public ArrayList<Attribut> a(ArrayList<Objet> objs){
		if(objs.isEmpty()) {return attributs;}
		ArrayList<ArrayList<Attribut>> l=new ArrayList<ArrayList<Attribut>>();
		for(Objet o : objs) {
			ArrayList<Attribut> att=contexte.get(o);
			l.add(att);
		}
		return interrec(l);
	}
	/**
	 * @brief Applique l'opération A sur une liste d'objets
	 * @param objs un tableau contenant les nom des objets sur lesquels on veut effectuer l'opération A
	 * @return la liste des attributs vérifiés par tous les objets correspondant aux noms contenus dans objs
	 */
	public ArrayList<Attribut> a(String[] objs){
		ArrayList<Objet> temp=new ArrayList<Objet>();
		for(String s : objs) {
			Objet o=getObjet(s);
			temp.add(o);
		}
		return a(temp);
	}
	/**
	 * @brief Applique l'opération E sur une liste d'attributs
	 * @param atts une liste d'attributs sur laquelle on veut effectuer l'opération E
	 * @return la liste d'objets vérifiant tous les attributs de atts
	 */
	public ArrayList<Objet> e(ArrayList<Attribut> atts){
		if(atts.isEmpty()) {return objets;}
		ArrayList<Objet> res=new ArrayList<Objet>();
		for(Objet o : objets) {
			boolean b=true;
			for(Attribut a : atts) {
				if(!contexte.get(o).contains(a)) {b=false;}
			}
			if(b) {res.add(o);}
		}
		return res;
	}
	/**
	 * @brief Applique l'opération E sur une liste d'attributs
	 * @param atts un tableau contenant les nom des attributs sur lesquels on veut effectuer l'opération E
	 * @return la liste des objets vérifiant par tous les attributs correspondant aux noms contenus dans atts
	 */
	public ArrayList<Objet> e(String[] atts){
		ArrayList<Attribut> temp=new ArrayList<Attribut>();
		for(String s : atts) {
			Attribut a = getAttribut(s);
			temp.add(a);
		}
		return e(temp);
	}
	/**
	 * @brief Permet de déterminer en temps polynomial si un attribut est polynomial ou non
	 * @param a un attribut dont on veut savoir s'il est irréductible ou non
	 * @return true si a est irréductible dans le contexte, false sinon
	 */
	public boolean irr(Attribut a){
		ArrayList<Attribut> f=a.fermeture();
		f=sansDoublon(f);
		ArrayList<Objet> et=disjoin(objets, a.e());
		ArrayList<Attribut> fusion=new ArrayList<Attribut>();
		for(Attribut ai : f) {
			if(et.isEmpty()) { return false;}
			if(egal(a.e(), ai.e())){fusion.add(ai);}
			if(!contient(fusion, ai)) {et=inter(et, ai.e());}
		}
		return !et.isEmpty();
	}

	/**
	 * @brief Permet d'obtenir les attributs irréductibles dans le contexte
	 * @return la liste d'attributs irréductibles dans le contexte
	 */
	public ArrayList<Attribut> irreductibles(){
		ArrayList<Attribut> res=new ArrayList<Attribut>();
		for(Attribut a : attributs) {
			if(irr(a)) {
				if(!a.e().isEmpty() && !egal(a.e(),objets)) {res.add(a);}
			}
		}
		return res;
	}
	/**
	 * @brief Donne une liste d'attributs irréductibles dans le contexte
	 * @return une liste d'attributs irréductibles dans le contexte
	 */
	public ArrayList<Attribut> distIrr(){
		ArrayList<Attribut> irreductibles=irreductibles();
		ArrayList<Attribut> doublon=new ArrayList<Attribut>();
		int i=0;
		while(i<irreductibles.size()) {
			Attribut ai=irreductibles.get(i);
			if(!contient(doublon, ai)) {
				int j=i+1;
				while(j<irreductibles.size()) {
					Attribut aj=irreductibles.get(j);
					if(!contient(doublon, aj) && egal(ai.e(), aj.e())) {doublon.add(aj);}
					j=j+1;
				}
			} 
			i=i+1;
		}
		return disjoin(irreductibles, doublon);
	}
	/**
	 * @brief Permet de simplifier une relation grâce aux relations déjà obtenus
	 * @details On s'intéresse à une liste d'atome et on regarde si dans cette liste, certains atomes impliquent d'autres atomes également présent dans la liste. Si c'est le cas, on élimine les atomes impliqués
	 * @param r Une map donnant les relations de la forme cle => val1, val2... valn
	 * @param liste une liste d'attributs que l'on va chercher à simplifier grâce aux relations contenus dans r
	 * @return La liste d'attributs l dont on a retiré les attributs impliqués par d'autres attributs de l
	 */
	public ArrayList<Attribut> simplification(HashMap<Attribut, ArrayList<Attribut>> r, ArrayList<Attribut> liste){
		//Itère sur les attributs de la liste, on regarde si on peut simplifier cette attribut
		ArrayList<Attribut>l=new ArrayList<Attribut>();
		for(Attribut a : liste) {
			ArrayList<Attribut> atomes=new ArrayList<Attribut>();
			//on crée une liste contenant les atomes de clés du dictionnaire de règle
			for(Attribut e : r.keySet()) {atomes.add(e);}
			//Si le dictionnaire de règle contient l'atome a en prémisce, et qu'il n'a pas déjà été simplifié, on peut simplifier par a
			if(contient(atomes, a) && !contient(l , a)) {
				//On enlève tous les atomes impliqués par a
				if(r.get(a)!=null)
				for(Attribut atome : r.get(a)) {
					if(!contient(l, atome)) {l.add(atome);}
				}
			}
		}
		return disjoin(liste, l);
	}
	/**
	 * @brief Donne une map donnant des relations entre attributs tel que cle => val1, val2...valn
	 * @param pre Une liste d'attributs qui seront en prémisce des relations (les clés de la map renvoyée)
	 * @param post Une liste d'attributs qui seront en conclusion des relations (les valeurs de la map renvoyée)
	 * @return Une carte donnant des relations entre attributs tel que cle => val1, val2,... valn
	 */
	public HashMap<Attribut, ArrayList<Attribut>> keyImpliesValues(ArrayList<Attribut> pre, ArrayList<Attribut> post){
		HashMap<Attribut, ArrayList<Attribut>> map=new HashMap<Attribut, ArrayList<Attribut>>();
		for(Attribut a : pre) {
			ArrayList<Attribut> f=a.fermeture();
			ArrayList<Attribut> r=new ArrayList<Attribut>();
			for(Attribut ai : f) {
				if(contient(post, ai)) {r.add(ai);}
			}
			if(!r.isEmpty()) {
				map.put(a, r);
			}
		}
		return map;
	}
	/**
	 * @brief Simplifie les relations représentés dans une map à l'aide de relations présentes dans une autre map
	 * @note Rien n'empêche les deux map données en paramètre d'être identiques
	 * @param r Une map contenant des relations entre attributs de la forme cle => val1, val2...valn
	 * @param i La map contenant des relations entre attributs qu'on souhaite simplifier
	 * @return la map i exprimant des relations entre attributs simplifiées
	 */
	public HashMap<Attribut, ArrayList<Attribut>> simpleTab(HashMap<Attribut, ArrayList<Attribut>> r, HashMap<Attribut, ArrayList<Attribut>> i){
		HashMap<Attribut, ArrayList<Attribut>>res=i;
		Set<Attribut> lc=i.keySet();
		for(Attribut a : lc) {
			ArrayList<Attribut> l=i.get(a);
			l=simplification(r, l);
			res.put(a, l);
		}
		return res;
	}
	/**
	 * @brief Donne l'attribut d'une liste d'attribut donnée en paramètre qui couvre le plus grand nombre d'objets parmi une liste elle aussi passée en paramètre
	 * @note Un attribut a couvre un ensemble d'objet o si objets-E(a) = o
	 * @param couvrir La liste d'objets à couvrir
	 * @param couvreur la liste des attributs parmi lesquels on sélectionne l'attribut qui couvre le plus d'objets
	 * @return L'attribut parmi couvreur qui couvre le plus d'objets de la liste couvrir, null si aucun attribut ne couvre ne serait-ce qu'un objet de couvrir
	 */
	public Attribut couvreurMax(ArrayList<Objet> couvrir, ArrayList<Attribut> couvreur) {
		Attribut maxim=attributs.get(0);
		int n=0;
		for(Attribut a : couvreur) {
			ArrayList<Objet> couverture=disjoin(objets, a.e());
			ArrayList<Objet> intersec=inter(couverture, couvrir);
			int x=intersec.size();
			if(x>n) {
				n=x;
				maxim=a;
			}
		}
		if(n==0) {return null;}
		else {
			return maxim;
		}
	}
	/**
	 * @brief Renvoie un ensemble d'attributs qui couvrent un ensemble d'objets passé en paramètre.
	 * @note L'ensemble d'attribut est sélectionné parmi une liste d'attributs passée en paramètre
	 * @param couvrir la liste des objets à couvrir
	 * @param couvreur la liste des attributs parmi lesquels on sélectionne les attributs qui couvrent les objets de couvrir
	 * @return La liste représentant le sous-ensemble de couvreur qui couvrent tous les objets de couvrir ou une liste vide si un tel sous-ensemble n'existe pas
	 */
	public ArrayList<Attribut> couverture(ArrayList<Objet> couvrir, ArrayList<Attribut> couvreur){
		ArrayList<Attribut> l=new ArrayList<Attribut>();
		ArrayList<Objet> couv=couvrir;
		while(!couv.isEmpty()) {
			Attribut c=couvreurMax(couv, couvreur);
			if(c==null) {return new ArrayList<Attribut>();}
			else {
				l.add(c);
				ArrayList<Objet> o=disjoin(objets, c.e());
				couv=disjoin(couv, o);
			}
		}
		return l;
	}
	/**
	 * @brief Permet de créer une map exprimant des relations entre attributs de la forme val1, val2...valn => cle
	 * @param pre la liste d'attribut qui seront en prémisce de la relation
	 * @param post la liste d'attribut qui seront en conclusion de la relation
	 * @return Une map donnant des relations entre attributs de la forme val1, val2...valn => cle
	 */
	public HashMap<Attribut, ArrayList<Attribut>> valuesImpKey(ArrayList<Attribut> pre, ArrayList<Attribut> post){
		HashMap<Attribut, ArrayList<Attribut>> dico=new HashMap<Attribut, ArrayList<Attribut>>();
		for(Attribut a : post) {
			ArrayList<Attribut> atts=enleve(pre, a);
			ArrayList<Objet> o=disjoin(objets, a.e());
			ArrayList<Attribut> l=couverture(o,atts);
			if(!l.isEmpty()) {
				dico.put(a, l);
			}
		}
		return dico;
	}
	/**
	 * @brief Donne une liste d'attribut qui couvre au moins en partie un ensemble d'objets donné en paramètre
	 * @note Les attributs sélectionnés sont sélectionnés parmi une liste elle aussi passée en paramètre
	 * @param couvrir Une liste d'objets à couvrir
	 * @param couvreur Une liste d'attributs parmi lesquels on va sélectionner les attributs couvrant au moins en partie les objets de couvrir
	 * @return Un sous-ensemble d'attributs présents dans couvreur qui couvrent au moins en partie l'ensemble des objets présents dans couvrir
	 */
	public ArrayList<Attribut> couvreurList(ArrayList<Objet> couvrir, ArrayList<Attribut> couvreur){
		ArrayList<Attribut> l=new ArrayList<Attribut>();
		for(Attribut a : couvreur) {
			ArrayList<Objet> couverture=disjoin(objets, a.e());
			if(!inter(couvrir, couverture).isEmpty()) {l.add(a);}
		}
		return l;
	}
	/**
	 * @brief Donne une autre map exprimant des relations de la forme val1, val2...valn => cle
	 * @note Il s'agit ici d'un dictionnaire donnant pour chaque attribut, une liste de relation associée. Soit une liste de listes d'attributs
	 * @param pre Une liste d'attributs qui seront en prémisce des relations
	 * @param post Une liste d'attributs qui seront en conclusion des relations
	 * @return Une map donnant pour différents attributs a, une liste de relations de la forme val1, val2...valn => a
	 */
	public HashMap<Attribut, ArrayList<ArrayList<Attribut>>> valuesImpKeySup(ArrayList<Attribut> pre, ArrayList<Attribut> post){
		HashMap<Attribut, ArrayList<ArrayList<Attribut>>> dico=new HashMap<Attribut, ArrayList<ArrayList<Attribut>>>();
		for(Attribut a : post) {
			ArrayList<Attribut> atts=enleve(pre, a);
			ArrayList<Objet> couvrir=disjoin(objets, a.e());
			ArrayList<Attribut> listCouvreur=couvreurList(couvrir, atts);
			for(Attribut ai : listCouvreur) {
				ArrayList<Objet> o=disjoin(couvrir, disjoin(objets, ai.e()));
				if(o.isEmpty()) {
					ArrayList<Attribut>  t1=new ArrayList<Attribut>(); t1.add(ai);
					if(!dico.containsKey(a)) {
						ArrayList<ArrayList<Attribut>> t2=new ArrayList<ArrayList<Attribut>>(); t2.add(t1);
						dico.put(a, t2);
					}
					else {
						ArrayList<ArrayList<Attribut>> t2=dico.get(a); t2.add(t1);
						dico.put(a, t2);
					}
				}
				else {
					ArrayList<Attribut> couvreur=enleve(atts, ai);
					ArrayList<Attribut> l=couverture(o, couvreur);
					if(!l.isEmpty()) {
						l.add(ai);
						if(!dico.containsKey(a)) {
							ArrayList<ArrayList<Attribut>> t=new ArrayList<ArrayList<Attribut>>(); t.add(l);
							dico.put(a, t);
						}
						else {
							boolean b=false;
							for(ArrayList<Attribut> la : dico.get(a)) {if(egal(la, l)) {b=true;}}
							if(!b) {
								ArrayList<ArrayList<Attribut>> t=dico.get(a); t.add(l);
								dico.put(a, t);
							}
						}
					}
				}
			}
		}
		return dico;
	}
	/**
	 * @brief Donne les objets indexés par un attribut
	 * @param a l'attribut dont on souhaite récupérer les objets indexés dans le contexte
	 * @return Une liste d'objets qui vérifient l'attribut a
	 * 
	 */
	public ArrayList<Objet> objetIndex(Attribut a) {
		ArrayList<Objet> l=revContexte.get(a.toString());
		if(l==null) {return new ArrayList<Objet>();}
		else{return l;}
	}
}
