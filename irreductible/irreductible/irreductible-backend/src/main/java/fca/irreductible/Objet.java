package fca.irreductible;

import java.util.ArrayList;

/**
 * 
 * @author CAMARAZO David
 * @class Objet
 * @brief Cette classe représente un objet dans un contexte FCA
 * @details 3 attributs, le contexte auquel appartient l'objet, un nom et un numéro
 */

public class Objet extends AbstractElement{
	private Contexte contexte;
	private String nom;
	private int numero;
	
	/**
	 * @brief Le constructeur de la class Objet
	 * @param c le contexte auquel appartient l'objet
	 * @param s le nom de l'objet
	 * @param i le numéro de l'objet (son indice)
	 */
	public Objet(Contexte c, String s, int i) {
		contexte=c;
		nom=s;
		numero=i;
	}
	/**
	 * @brief Accesseur permettant d'obtenir le contexte auquel appartient l'objet
	 * @return contexte, le contexte auquel appartient l'objet
	 * 
	 */
	public Contexte contexte() {
		return contexte;
	}
	
	/**
	 * @brief Surcharge de la méthode toString pour l'affichage et l'écriture
	 * @return nom, le nom de l'objet
	 */
	public String toString() {
		return nom;
	}
	/**
	 * @brief Accesseur permettant d'obtenir le nom de l'objet
	 * @return nom, le nom de l'objet
	 */
	public String nom() {
		return nom;
	}
	/**
	 * @brief Accesseur permettant d'obtenir le numér de l'objet
	 * @return numero le numéro de l'objet
	 */
	public int numero() {
		return numero;
	}
	/**
	 * @brief effectue l'opération A sur l'objet.
	 * @return La liste des attributs que l'objet appelant vérifie dans son contexte.
	 */
	public ArrayList<Attribut> a(){
		return contexte.get(this);
	}
}
