package fca.irreductible;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.io.File;
import java.io.FileWriter;
import java.util.Collections;
import java.util.Set;
/**
 * 
 * @author CAMARAZO David
 * @class Ecriture
 * @brief Cette classe fournit les méthodes pour stocker et écrire les résultats issues de l'analyse d'un contexte
 * @details 5 attributs, 5 Maps permettant de stocker les différentes relations trouvées grâce à l'analyse du contexte.
 * @details 3 listes classant les attributs (irréductibles, irréductibles distincts, non-irréductibles)
 */
public class Ecriture extends AbstractFCA{
	private HashMap<Attribut, ArrayList<Attribut>> cleImpVal=new HashMap<Attribut, ArrayList<Attribut>>();
	private HashMap<Attribut, ArrayList<Attribut>> equivalences=new HashMap<Attribut, ArrayList<Attribut>>();
	private HashMap<Attribut, ArrayList<Attribut>> valImpCle=new HashMap<Attribut, ArrayList<Attribut>>();
	private HashMap<Attribut, ArrayList<ArrayList<Attribut>>> valImpCleS= new HashMap<Attribut, ArrayList<ArrayList<Attribut>>>();
	private HashMap<Attribut, ArrayList<Objet>> atomique=new HashMap<Attribut, ArrayList<Objet>>();
	private ArrayList<Attribut> irreduc=new ArrayList<Attribut>();
	private ArrayList<Attribut> nonIrr=new ArrayList<Attribut>();
	private ArrayList<Attribut> dist_irr=new ArrayList<Attribut>();

	/**
	 * @brief Construit l'objet qui va écrire les résultat dans un format choisi
	 * @param contexte le contexte analysé
	 */
	 public Ecriture(Contexte contexte) {
		 //Trois listes d'attributs : une pour les irréductibles distincts, une pour les irréductibles et une pour les non-irréductibles
		irreduc=contexte.irreductibles();
		nonIrr=contexte.disjoin(contexte.attributs(), irreduc);
		dist_irr=contexte.distIrr();
		Collections.sort(dist_irr);
		/*
		 * 5 Map pour 5 relations :
		 * a=>ai..an
		 * a<=>ai..an
		 * ai..an=>a
		 * oi...ok => om...on
		 * ai..an=>a sup
		 */
		cleImpVal=contexte.keyImpliesValues(dist_irr, irreduc);
		equivalences=contexte.keyImpliesValues(nonIrr, irreduc);
		valImpCle=contexte.valuesImpKey(dist_irr, dist_irr);
		valImpCleS=contexte.valuesImpKeySup(dist_irr, dist_irr);
		
		valImpCle=contexte.simpleTab(cleImpVal, valImpCle);
		cleImpVal=contexte.simpleTab(cleImpVal, cleImpVal);
		equivalences=contexte.simpleTab(equivalences, equivalences);
		
		//indivisibles
		atomique=new HashMap<Attribut, ArrayList<Objet>>();
		for(Attribut a : cleImpVal.keySet()) {
			ArrayList<Objet> l=new ArrayList<Objet>();
			if(atomique.containsKey(a)) {l=atomique.get(a);}
			for(Attribut ai : cleImpVal.get(a)) {
				ArrayList<Objet> lo1=a.e();
				ArrayList<Objet> lo2=ai.e();
				for(Objet o : lo2) {
					if(!contient(l, o) && !contient(lo1, o)) l.add(o);
				}
				if(!l.isEmpty() && !lo1.isEmpty())atomique.put(a, l);
			}
		}
	 }
	 /**
	  * @brief écris les résultats de l'analyse du contexte dans un fichier json 
	  * @throws IOException
	  */
	 public void ecrireJson() throws IOException{
		 String jsonName="Resultats.json";
		File json=new File(jsonName);
		json.createNewFile();
		FileWriter jsonWriter=new FileWriter(jsonName);
		int taille=irreduc.size()+nonIrr.size();
		String ecrire="{\n  \"attributs\" : "+taille+",\n  "+
					"\"irreductibles\" : "+irreduc.size()+",\n  "+
					"\"irreductibles distincts\" : "+dist_irr.size()+",\n  "+
					"\"cle implique valeurs\" : {\n    ";
		jsonWriter.write(ecrire);
		int compt=0;
		for(Attribut a : cleImpVal.keySet()) {
			compt=compt+1;
			ecrire="\""+a.toString()+"\" : [";
			ArrayList<Attribut> values=cleImpVal.get(a);
			Iterator<Attribut> it=values.iterator();
			while(it.hasNext()) {
				Attribut valeur=it.next();
				ecrire=ecrire+"\""+valeur.toString()+"\"";
				if(it.hasNext()) {ecrire=ecrire+", ";}
			}
		if(compt!=cleImpVal.keySet().size()) {ecrire=ecrire+"],\n    "; jsonWriter.write(ecrire);}
		else {ecrire=ecrire+"]\n  },\n  \"cle equivaut valeurs\" : {\n    "; jsonWriter.write(ecrire);}
		}
		compt=0;
		for(Attribut a : equivalences.keySet()) {
			compt=compt+1;
			ecrire="\""+a.toString()+"\" : [";
			ArrayList<Attribut> values=equivalences.get(a);
			Iterator<Attribut> it=values.iterator();
			while(it.hasNext()) {
				Attribut valeur=it.next();
				ecrire=ecrire+"\""+valeur.toString()+"\"";
				if(it.hasNext()) {ecrire=ecrire+", ";}
			}
			if(compt!=equivalences.keySet().size()) {ecrire=ecrire+"],\n    "; jsonWriter.write(ecrire);}
			else {ecrire=ecrire+"]\n  },\n  \"valeurs impliquent cle\" : {\n    "; jsonWriter.write(ecrire);}
		}
		compt=0;
		for(Attribut a : valImpCle.keySet()) {
			compt=compt+1;
			ecrire="\""+a.toString()+"\" : [";
			ArrayList<Attribut> values=valImpCle.get(a);
			Iterator<Attribut> it=values.iterator();
			while(it.hasNext()) {
				Attribut valeur=it.next();
				ecrire=ecrire+"\""+valeur.toString()+"\"";
				if(it.hasNext()) {ecrire=ecrire+", ";}
			}
			if(compt!=valImpCle.keySet().size()) {ecrire=ecrire+"],\n    "; jsonWriter.write(ecrire);}
			else {ecrire=ecrire+"]\n  },\n  \"indivisibles\" : {\n    "; jsonWriter.write(ecrire);}
		}
		compt=0;
		for(Attribut a : atomique.keySet()) {
			compt=compt+1;
			ecrire="";
			ArrayList<Objet> lo1=a.e();
			ArrayList<Objet> lo2=atomique.get(a); Iterator<Objet> it2=lo2.iterator();
			while(it2.hasNext()) {
				Objet val2=it2.next(); ecrire=ecrire+"\""+val2.toString()+"\""+" : [";
				Iterator<Objet> it1=lo1.iterator();
				while(it1.hasNext()) {
					Objet val1=it1.next();
					ecrire=ecrire+"\""+val1.toString()+"\"";
					if(it1.hasNext()) {ecrire=ecrire+", ";}
					else {ecrire=ecrire+"]";}
				}
				if(it2.hasNext()) {ecrire=ecrire+",\n    ";}
			}
			if(compt!=atomique.keySet().size()) {ecrire=ecrire+",\n    ";}
			jsonWriter.write(ecrire);
		}
		ecrire="\n  },\n  \"valeurs impliquent cle Sup\" : {\n    "; jsonWriter.write(ecrire);
		int dt=0;
		for(Attribut a : valImpCleS.keySet()) {for(ArrayList<Attribut> l : valImpCleS.get(a)) {dt=dt+1;}}
		compt=0;
		for(Attribut a : valImpCleS.keySet()) {
			compt=compt+1;
			ecrire="\""+a.toString()+"\" : [[";
			int compt1=0;
			for(ArrayList<Attribut> l : valImpCleS.get(a)) {
				compt1=compt1+1;
				Iterator<Attribut> it=l.iterator();
				while(it.hasNext()) {
					Attribut valeur=it.next();
					ecrire=ecrire+"\""+valeur.toString()+"\"";
					if(it.hasNext()) {ecrire=ecrire+", ";}
				}
				if(compt1!=valImpCleS.get(a).size()) {ecrire=ecrire+"], [";}
				else {ecrire=ecrire+"]]"; jsonWriter.write(ecrire);}
			}
			if(compt!=valImpCleS.keySet().size()) {ecrire=",\n    "; jsonWriter.write(ecrire);}
			else {ecrire="\n  }\n}"; jsonWriter.write(ecrire);}
		}
		jsonWriter.close();
	 }
	 /**
	  * @brief Ecris les résultats de l'analyse du contexte dans un fichier txt
	  * @param mode indique dans quel format on écrit les règles, si mode = d, les règles seront décomposés. Sinon on écrit avec le format par défaut
	  * @throws IOException
	  */
	 public void ecrireTxt(String mode) throws IOException{
		String filename="irreductibles.txt";
		File file=new File(filename);
		file.createNewFile();
		FileWriter writer=new FileWriter(filename);
		int taille=irreduc.size()+nonIrr.size();
		String s="nombre d'attributs : "+taille+"\nnombre d'irréductibles : "+irreduc.size()+
					"\nnombre d'irréductibles distincts : "+dist_irr.size()+"\n\n";
		writer.write(s);
		s="";
		for(Attribut a : dist_irr) {
			s=s+a.toString()+"\n";
		}
		s=s+"\nnombre de relations a=> ai...an : "+cleImpVal.size()+"\n";
		writer.write(s);
		Set<Attribut> ks;
		ArrayList<Attribut> la;
		if(mode.equals("d")) {
			s="";
			ks=cleImpVal.keySet();
			la=triSet(ks);
			for(Attribut a : la) {
				ArrayList<Attribut> values=cleImpVal.get(a);  Collections.sort(values);
				for(Attribut ai : values) {
					s=s+a.toString()+" => "+ai.toString()+"\n";
				}
			}
			writer.write(s);
		}
		else {
			ks=cleImpVal.keySet();
			la=triSet(ks);
			for(Attribut a : la) {
				s=a.toString()+" => ";
				ArrayList<Attribut> values=cleImpVal.get(a); Collections.sort(values);
				Iterator<Attribut> it=values.iterator();
				while(it.hasNext()) {
					Attribut valeur=it.next();
					s=s+valeur.toString();
					if(it.hasNext()) s=s+", ";
				}
				s=s+"\n"; writer.write(s);
			}
		}
		s="\nnombre de relations a <=> ai...an : "+equivalences.size()+"\n";
		writer.write(s);
		ks=equivalences.keySet(); la=triSet(ks);
		for(Attribut a : la) {
			s=a.toString()+" <=> ";
			ArrayList<Attribut> values=equivalences.get(a); Collections.sort(values);
			Iterator<Attribut> it=values.iterator();
			while(it.hasNext()) {
				Attribut valeur=it.next();
				s=s+valeur.toString();
				if(it.hasNext()) s=s+", ";
			}
			s=s+"\n"; writer.write(s);
		}
		s="\nnombre de relations ai...an => a : "+valImpCle.size()+"\n";
		writer.write(s);
		ks=valImpCle.keySet(); la=triSet(ks);
		for(Attribut a : la) {
			s="";
			ArrayList<Attribut> values=valImpCle.get(a); Collections.sort(values);
			Iterator<Attribut> it=values.iterator();
			while(it.hasNext()) {
				Attribut valeur=it.next();
				s=s+valeur.toString();
				if(it.hasNext()) s=s+", ";
			}
			s=s+" => "+a.toString();
			s=s+"\n"; writer.write(s);
		}
		s="\nindivisibles : "+atomique.size()+"\n";
		writer.write(s);
		ks=atomique.keySet(); la=triSet(ks);
		if(mode.equals("d")) {
			for(Attribut a : la) {
				s="";
				String pre="";
				ArrayList<Objet> lo1=a.e(); Collections.sort(lo1); Iterator<Objet> it1=lo1.iterator();
				while(it1.hasNext()) {Objet val=it1.next(); pre=pre+val.toString(); if(it1.hasNext()) {pre=pre+", ";}}
				pre=pre+" => ";
				ArrayList<Objet> lo2=atomique.get(a); Collections.sort(lo2); Iterator<Objet> it2=lo2.iterator();
				while(it2.hasNext()) {Objet val=it2.next(); s=s+pre+val.toString()+"\n";}
				writer.write(s);
			}
		}
		else {
			for(Attribut a : la) {
				s="";
				ArrayList<Objet> lo1=a.e(); Collections.sort(lo1); Iterator<Objet> it1=lo1.iterator();
				while(it1.hasNext()) {Objet val=it1.next(); s=s+val.toString(); if(it1.hasNext()) {s=s+", ";}}
				s=s+" => ";
				ArrayList<Objet> lo2=atomique.get(a); Collections.sort(lo2); Iterator<Objet> it2=lo2.iterator();
				while(it2.hasNext()) {Objet val=it2.next(); s=s+val.toString(); if(it2.hasNext()) {s=s+", ";}}
				s=s+"\n";
				writer.write(s);
			}
		}
		int dt=0;
		for(Attribut a : valImpCleS.keySet()) {for(ArrayList<Attribut> l : valImpCleS.get(a)) {dt=dt+1;}}
		s="\nnombre de relations supplémentaires ai...an => a : "+dt+"\n";
		writer.write(s);
		ks=valImpCleS.keySet(); la=triSet(ks);
		for(Attribut a : la) {
			for(ArrayList<Attribut> l : valImpCleS.get(a)) {
				s="";
				Iterator<Attribut> it=l.iterator();
				while(it.hasNext()) {
					Attribut valeur=it.next();
					s=s+valeur.toString();
					if(it.hasNext()) {s=s+", ";}
				}
				s=s+" => "+a.toString()+"\n";
				writer.write(s);
			}
		}
		writer.close();
	 }
}
