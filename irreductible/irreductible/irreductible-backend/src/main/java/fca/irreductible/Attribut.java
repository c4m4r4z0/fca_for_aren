package fca.irreductible;
import java.util.ArrayList;
import java.util.Arrays;
/**
 * 
 * @author CAMARAZO David
 * @class Attribut
 * @brief Cette classe représente un attriubt dans un contexte FCA
 * @details 3 attributs, le contexte auquel appartient l'attribut, un nom et un numéro
 */
public class Attribut extends AbstractElement{
	private Contexte contexte;
	private String nom;
	private int numero;
	/**
	 * @brief Le constructeur de la class Attribut
	 * @param c le contexte auquel appartient l'attribut
	 * @param s le nom de l'attribut
	 * @param i le numéro de l'attribut (son indice)
	 */
	public Attribut(Contexte c, String s, int i) {
		contexte=c;
		nom=s;
		numero=i;
	}
	/**
	 * @brief Surcharge de la méthode toString pour l'affichage et l'écriture
	 * @return nom, le nom de l'attribut
	 */
	public String toString() {
		return nom;
	}
	/**
	 * @brief Accesseur permettant d'obtenir le contexte auquel appartient l'attribut
	 * @return contexte, le contexte auquel appartient l'attribut
	 */
	public Contexte contexte() {
		return contexte;
	}
	/**
	 * @brief Accesseur permettant d'obtenir le nom de l'attribut
	 * @return nom, le nom de l'attribut
	 */
	public String nom() {
		return nom;
	}
	/**
	 * @brief Accesseur permettant d'obtenir le numéro de l'attribut
	 * @return numero, le numéro de l'attribut
	 */
	public int numero() {
		return numero;
	}
	/**
	 * @brief Effectue l'opération E sur l'attribut
	 * @return La liste d'objets vérifiés par l'attribut appelant dans son contexte
	 */
	public ArrayList<Objet> e(){
		return contexte.objetIndex(this);
	}
	/**
	 * @brief Effectue l'opération de fermeture sur l'attribut
	 * @details Cela revient à faire A(E(this)). La fermeture est bien sûr lié au contexte de l'attribut.
	 * @return La liste d'attributs contenus dans le fermeture de l'attribut appelant.
	 */
	public ArrayList<Attribut> fermeture(){
		ArrayList<Objet> obj=e();
		ArrayList<Attribut> att=contexte.a(obj);
		ArrayList<Attribut> tempo= new ArrayList<Attribut>(Arrays.asList(this));
		return disjoin(att, tempo);
	}
}
